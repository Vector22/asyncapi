# from starlette.testclient import TestClient

# from app.main import app

# # Instantiate a new test client
# client = TestClient(app)


def test_ping(test_app):
    # Use the client instanciated in the conftest file
    response = test_app.get("/ping")
    assert response.status_code == 200
    assert response.json() == {"ping": "pong!"}
