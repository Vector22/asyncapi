import pytest
from starlette.testclient import TestClient

from app.main import app

# Create a test client fixture
@pytest.fixture(scope="module")
def test_app():
    client = TestClient(app)
    yield client
