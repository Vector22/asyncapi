"""
    This module is the FastApi entry point
"""

from fastapi import FastAPI

from app.api import notes, ping
from app.db import engine, metadata, database

# Create the DB right now
metadata.create_all(engine)

app = FastAPI()


# Connect to the db asynchronously
@app.on_event("startup")
async def startup():
    await database.connect()


# disconnect to the db asynchronously
@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


# Register the ping route in the router
app.include_router(ping.router)

# Register the notes route in the router
app.include_router(notes.router, prefix="/notes", tags=["notes"])
