from app.api.models import NoteSchema
from app.db import notes, database


# Insert a note in the database
async def post(payload: NoteSchema):
    # pylint: disable=no-value-for-parameter
    query = notes.insert().values(title=payload.title, description=payload.description)
    return await database.execute(query=query)


# Retrieve a note from the database
async def get(id: int):
    query = notes.select().where(id == notes.c.id)
    return await database.fetch_one(query=query)


# Retrieve multyple notes from the database
async def get_all():
    query = notes.select()
    return await database.fetch_all(query=query)


# Update a note in the database
async def put(id: int, payload: NoteSchema):
    # pylint: disable=no-value-for-parameter
    query = (
        notes
        .update()
        .where(id == notes.c.id)
        .values(title=payload.title, description=payload.description)
        .returning(notes.c.id)
    )
    return await database.execute(query=query)


# Delete a note from the database
async def delete(id: int):
    # pylint: disable=no-value-for-parameter
    query = notes.delete().where(id == notes.c.id)
    return await database.execute(query=query)
