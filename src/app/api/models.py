from pydantic import BaseModel, Field


class NoteSchema(BaseModel):
    # Specify a min and max length for the attributs
    title: str = Field(..., min_length=3, max_length=50)
    description: str = Field(..., min_length=3, max_length=50)


class NoteDB(NoteSchema):
    id: int
